-------------------------------------------------------------------------------
--
-- Title       : lab1
-- Design      : lab1
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab1\lab1\src\lab1.vhd
-- Generated   : Wed Mar 20 09:54:55 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {lab1} architecture {lab1}}

library IEEE;
use IEEE.std_logic_1164.all; 
--use IEEE.std_logic_unsigned.all;

entity lab1 is
	 port(
		 R : in STD_LOGIC;
		 S : in STD_LOGIC;
		 Q : out STD_LOGIC;
		 NQ : out STD_LOGIC
	     );
end lab1;

--}} End of automatically maintained section

architecture lab1 of lab1 is   
signal p: std_logic;
begin
	
	process(R,S)
	begin
		if (S='1' and R='0') then
			p <= '1';
        elsif R='1' then
		p <= '0';
		end if;
	end process;	
	Q <= p;
	NQ <= not p;
	 --NQ <= '0' when S='1' else
         --'1' when R='1' and S='0';

end lab1;
