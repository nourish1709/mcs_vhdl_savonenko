-------------------------------------------------------------------------------
--
-- Title       : lab1_2
-- Design      : lab1
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab1\lab1\src\lab1_2.vhd
-- Generated   : Wed Mar 20 10:11:53 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {lab1_2} architecture {lab1_2}}

library IEEE;
use IEEE.std_logic_1164.all;	 
use ieee.std_logic_unsigned.all;

entity lab1_2 is
	 port(
		 CLK : in STD_LOGIC;
		 RST : in STD_LOGIC;
		 Q : out STD_LOGIC_VECTOR(3 downto 0)
	     );
end lab1_2;

--}} End of automatically maintained section

architecture lab1_2 of lab1_2 is	

signal cnt: std_logic_vector(3 downto 0); --���������� ����������	 

begin			
	--cnt <= "0000";   
	--Q <= "0000";
	process (clk,rst,cnt)
	begin		   
		if rst = '1' then
			cnt <= "0000";
		elsif rising_edge(clk) then
			cnt <= cnt+'1';
			end if;
   end process;	
Q <= cnt;
	 -- enter your statements here --

end lab1_2;
