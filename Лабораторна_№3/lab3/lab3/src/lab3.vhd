-------------------------------------------------------------------------------
--
-- Title       : lab3
-- Design      : lab3
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab3\lab3\src\lab3.vhd
-- Generated   : Tue Apr  9 15:39:18 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {lab3} architecture {lab3}}

library IEEE;
use IEEE.std_logic_1164.all;

entity lab3 is
	 port(
		 x : in STD_LOGIC_VECTOR(2 downto 0);
		 y : out STD_LOGIC
	     );
end lab3;

--}} End of automatically maintained section

architecture lab3 of lab3 is	
signal a, b, c, d, e, f: std_logic;
begin
	A <= not x(0) after 5 ns;
	B <= a or x(1) after 10 ns;
	C <= a nor x(2) after 10 ns;
	D <= B and C after 10 ns;
	E <= x(2) xnor C after 10 ns;
	F <= B xor D after 10 ns;
	Y <= F nand E after 10 ns;		  	   
end lab3;