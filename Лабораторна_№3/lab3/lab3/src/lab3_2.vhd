library IEEE;
use IEEE.std_logic_1164.all;

entity lab3 is
	 port(
		 x : in STD_LOGIC_VECTOR(2 downto 0);
		 y : out STD_LOGIC
	     );
end lab3;


architecture lab3 of lab3 is	
signal a, b, c, d, e, f: std_logic;
begin
	A <= transport not x(0) after 5 ns;
	B <= transport a or x(1) after 10 ns;
	C <= transport a nor x(2) after 10 ns;
	D <= transport B and C after 10 ns;
	E <= transport x(2) xnor C after 10 ns;
	F <= transport B xor D after 10 ns;
	Y <= transport F nand E after 10 ns;		  	   
end lab3;