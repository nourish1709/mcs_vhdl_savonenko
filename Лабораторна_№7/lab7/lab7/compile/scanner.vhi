component scanner
	port (
		clk: in STD_LOGIC;
		ret: in STD_LOGIC_VECTOR (0 to 3);
		keycode: out STD_LOGIC_VECTOR (0 to 7);
		strobe: out STD_LOGIC;
		scan: inout STD_LOGIC_VECTOR (0 to 3));
end component;


instance_name : scanner
( clk => ,
 keycode => ,
 ret => ,
 scan => ,
 strobe => );
