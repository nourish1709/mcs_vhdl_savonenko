-------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : lab7
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab7\lab7\compile\scanner.vhd
-- Generated   : Tue May  7 10:22:32 2019
-- From        : c:\My_Designs\lab7\lab7\src\scanner.asf
-- By          : FSM2VHDL ver. 5.0.7.2
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity scanner is 
	port (
		clk: in STD_LOGIC;
		ret: in STD_LOGIC_VECTOR (0 to 3);
		keycode: out STD_LOGIC_VECTOR (0 to 7);
		strobe: out STD_LOGIC;
		scan: inout STD_LOGIC_VECTOR (0 to 3));
end scanner;

architecture scanner_arch of scanner is

-- diagram signals declarations
signal cond: STD_LOGIC;

-- USER DEFINED ENCODED state machine: Sreg0
attribute ENUM_ENCODING: string;
type Sreg0_type is (
    S2, S3, S4, S5, S0, S1
);
attribute ENUM_ENCODING of Sreg0_type: type is
	"0 " &		-- S2
	"0 " &		-- S3
	"0 " &		-- S4
	"0 " &		-- S5
	"0 " &		-- S0
	"0" ;		-- S1

signal Sreg0, NextState_Sreg0: Sreg0_type;

-- Declarations of pre-registered internal signals

begin

-- FSM coverage pragmas
-- Aldec enum Machine_Sreg0 CURRENT=Sreg0
-- Aldec enum Machine_Sreg0 NEXT=NextState_Sreg0
-- Aldec enum Machine_Sreg0 INITIAL_STATE=S0
-- Aldec enum Machine_Sreg0 STATES=S1,S2,S3,S4,S5
-- Aldec enum Machine_Sreg0 TRANS=S0->S0,S0->S1,S1->S2,S1->S5,S2->S3,S2->S5,S3->S4,S3->S5,S4->S0,S4->S5,S5->S0,S5->S5


----------------------------------------------------------------------
-- Machine: Sreg0
----------------------------------------------------------------------
------------------------------------
-- Next State Logic (combinatorial)
------------------------------------
Sreg0_NextState: process (cond, ret, scan, Sreg0)
begin
	NextState_Sreg0 <= Sreg0;
	-- Set default values for outputs and signals
	scan <= "1111";
	keycode <= "00000000";
	strobe <= '0';
	cond <= (ret(0) or ret(1) or ret(2) or ret(3));
	case Sreg0 is
		when S2 =>
			scan <= "0010";
			if cond = '0' then
				NextState_Sreg0 <= S3;
			elsif cond = '1' then
				NextState_Sreg0 <= S5;
			end if;
		when S3 =>
			scan <= "0100";
			if cond = '0' then
				NextState_Sreg0 <= S4;
			elsif cond = '1' then
				NextState_Sreg0 <= S5;
			end if;
		when S4 =>
			scan <= "1000";
			if cond = '0' then
				NextState_Sreg0 <= S0;
			elsif cond = '1' then
				NextState_Sreg0 <= S5;
			end if;
		when S5 =>
			keycode <= ret & scan;
			strobe <= '1';
			if cond = '1' then
				NextState_Sreg0 <= S5;
			elsif cond = '0' then
				NextState_Sreg0 <= S0;
			end if;
		when S0 =>
			scan <= "1111";
			strobe <= '0';
			keycode <= "00000000";
			if cond = '1' then
				NextState_Sreg0 <= S1;
			elsif cond = '0' then
				NextState_Sreg0 <= S0;
			end if;
		when S1 =>
			scan <= "0001";
			if cond = '0' then
				NextState_Sreg0 <= S2;
			elsif cond = '1' then
				NextState_Sreg0 <= S5;
			end if;
--vhdl_cover_off
		when others =>
			null;
--vhdl_cover_on
	end case;
end process;

------------------------------------
-- Current State Logic (sequential)
------------------------------------
Sreg0_CurrentState: process (clk)
begin
	if clk'event and clk = '1' then
		Sreg0 <= NextState_Sreg0;
	end if;
end process;

end scanner_arch;
