-------------------------------------------------------------------------------
--
-- Title       : lab4
-- Design      : lab4
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab4\lab4\src\lab4.vhd
-- Generated   : Tue Apr  9 18:19:12 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {lab4} architecture {lab4}}

library IEEE;
use IEEE.std_logic_1164.all;

entity lab4 is
	 port(
		 CLK : in STD_LOGIC;
		 Bout : out STD_LOGIC;
		 Ainout : inout STD_LOGIC
	     );
end lab4;

--}} End of automatically maintained section

architecture lab4 of lab4 is
  --signal CLK : std_logic :='0';	  
  begin	   
 Pr_CLK: process(CLK)
  begin
    -- формування сигналу CLK
 end process Pr_CLK;
 Pr_A: process(CLK)
  begin
   if CLK'event and CLK ='1' then Ainout<='1' after 5 ns;
    elsif CLK'event and CLK='0' then Ainout<='0' after 5 ns; 
   end if;
 end process Pr_A;
 Pr_B: process (Ainout)
  begin
   if Ainout'event then Bout<= not Ainout;
   end if;
 end process Pr_B;	
end lab4;
