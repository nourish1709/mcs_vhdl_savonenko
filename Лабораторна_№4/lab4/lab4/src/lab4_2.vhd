-------------------------------------------------------------------------------
--
-- Title       : lab4_2
-- Design      : lab4
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab4\lab4\src\lab4_2.vhd
-- Generated   : Tue Apr  9 19:10:45 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {lab4_2} architecture {lab4_2}}

library IEEE;
use IEEE.std_logic_1164.all;		
use IEEE.std_logic_unsigned.all;

entity lab4_2 is
	 port(
		 Bout : out STD_LOGIC;
		 Ainout : inout STD_LOGIC
	     );
end lab4_2;


architecture lab4_2 of lab4_2 is
signal CLK : std_logic :='0';	  

begin			   
 Pr_CLK: process
  begin
    wait for 10 ns;
	CLK <= not CLK;
 end process Pr_CLK;
 
 Pr_A: process
 begin		
    wait on CLK;   
    Ainout <= CLK after 5 ns;	
	wait for 21 ns; 
 end process Pr_A;		  	  
 
 Pr_B: process
  begin
   wait until Ainout'event;
   Bout <= not Ainout;	
 end process Pr_B;

end lab4_2;
