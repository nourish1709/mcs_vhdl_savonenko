-------------------------------------------------------------------------------
--
-- Title       : lab2
-- Design      : lab2
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab2\lab2\src\lab2.vhd
-- Generated   : Fri Mar 22 19:06:52 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {lab2} architecture {lab2}}

library IEEE;
use IEEE.std_logic_1164.all; 
--use ieee.std_logic_unsigned.all;

entity lab2 is
	 port(
		 x : in STD_LOGIC_VECTOR(3 downto 0);
		 y : out STD_LOGIC_VECTOR(6 downto 0)
	     );
end lab2;

--}} End of automatically maintained section

architecture lab2 of lab2 is
begin
	with x select
			y <= "1110111" when "0000", 
			"0100100" when "0001",
			"1011101" when "0010",
			"1101101" when "0011", 
			"0100101" when "0111",
			"1111111" when "1000",
			"1101111" when "1001",
			"0000000" when others;		 


end lab2;
