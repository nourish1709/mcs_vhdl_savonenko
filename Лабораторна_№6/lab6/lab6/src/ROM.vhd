-------------------------------------------------------------------------------
--
-- Title       : ROM
-- Design      : lab6
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab6\lab6\src\ROM.vhd
-- Generated   : Thu Apr 11 15:45:05 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ROM} architecture {ROM}}

library IEEE;
use IEEE.std_logic_1164.all;

entity ROM is
	 port(
		 CEO : in STD_LOGIC;
		 Addr : in STD_LOGIC_VECTOR(3 downto 0);
		 Dout : out STD_LOGIC_VECTOR(3 downto 0)
	     );
end ROM;

--}} End of automatically maintained section

architecture ROM of ROM is		 
type mem is array (15 downto 0) of std_logic_vector(3 downto 0);
constant reg: mem :=
("0000", "0001", "0010", "0011",
 "0100", "0101", "0110", "0111",
 "1000", "1001", "1010", "1011",
 "1100", "1101", "1110", "1111");
begin		
	process(ceo, addr)
	begin
		if ceo'event and ceo = '1' then
			case addr is
				when "0000" => dout <= reg(15);	
				when "0001" => dout <= reg(14);
				when "0010" => dout <= reg(13);
				when "0011" => dout <= reg(12);
				when "0100" => dout <= reg(11);
				when "0101" => dout <= reg(10);
				when "0110" => dout <= reg(9);
				when "0111" => dout <= reg(8);
				when "1000" => dout <= reg(7);
				when "1001" => dout <= reg(6);
				when "1010" => dout <= reg(5);
				when "1011" => dout <= reg(4);
				when "1100" => dout <= reg(3);
				when "1101" => dout <= reg(2);
				when "1110" => dout <= reg(1);
				when "1111" => dout <= reg(0);
				when others => null;
			end case;
		else dout <= "ZZZZ";
		end if;
		end process;
end ROM;
