-------------------------------------------------------------------------------
--
-- Title       : \Register\
-- Design      : lab5
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab5\lab5\src\Register.vhd
-- Generated   : Wed Apr 10 12:28:49 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {\Register\} architecture {\Register\}}

library IEEE;
use IEEE.std_logic_1164.all;

entity \Register\ is
	 port(
		 clk : in STD_LOGIC;
		 we : in STD_LOGIC;
		 re : in STD_LOGIC;
		 data_in : in STD_LOGIC_VECTOR(7 downto 0);
		 data_out : out STD_LOGIC_VECTOR(7 downto 0)
	     );
end \Register\;


architecture \Register\ of \Register\ is	
begin			  
	process (clk)  
	variable mem: std_logic_vector(7 downto 0);
	begin	
		if clk'event and clk = '1' then
			if we = '1' and re = '0' then
				mem := data_in;
			elsif we = '0' and re = '1' then
				data_out <= mem; 
			else data_out <= "ZZZZZZZZ";
			end if;
			end if;
			end process;
end \Register\;
