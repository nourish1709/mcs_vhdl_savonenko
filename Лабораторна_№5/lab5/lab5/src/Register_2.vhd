-------------------------------------------------------------------------------
--
-- Title       : Register_2
-- Design      : lab5
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab5\lab5\src\Register_2.vhd
-- Generated   : Wed Apr 10 13:00:26 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Register_2} architecture {Register_2}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Register_2 is
	 port(
		 data_in : in STD_LOGIC;
		 clk : in STD_LOGIC;
		 we : in STD_LOGIC;
		 re : in STD_LOGIC;
		 data_out : out STD_LOGIC_VECTOR(0 to 7)
	     );
end Register_2;


architecture Register_2 of Register_2 is
begin
		process (clk)  
	variable mem: std_logic_vector(7 downto 0);
	begin	
		if clk'event and clk = '1' then
			if we = '1' and re = '0' then	
				mem(7) := data_in;
				for count in 7 downto 0 loop
					mem(count - 1) := mem(count);  
				end loop;
			elsif we = '0' and re = '1' then
				data_out <= mem; 
			else data_out <= "ZZZZZZZZ";
			end if;
			end if;
			end process;
end Register_2;
