-------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : lab8_2
-- Author      : Zhenia
-- Company     : Lviv Politechnic
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab8_2\lab8_2\compile\lab8_2.vhd
-- Generated   : Tue May 21 20:48:34 2019
-- From        : c:\My_Designs\lab8_2\lab8_2\src\lab8_2.bde
-- By          : Bde2Vhdl ver. 2.6
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_unsigned.all;

entity lab8_2 is
  port(
       CLK : in STD_LOGIC;
       WE : in STD_LOGIC;
       X : in STD_LOGIC_VECTOR(31 downto 0);
       LCD0 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD1 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD2 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD3 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD4 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD5 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD6 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD7 : out STD_LOGIC_VECTOR(6 downto 0)
  );
end lab8_2;

architecture lab8_2 of lab8_2 is

---- Component declarations -----

component Fub1
  port (
       CLK : in STD_LOGIC;
       WE : in STD_LOGIC;
       X : in STD_LOGIC_VECTOR(31 downto 0);
       Y : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component Fub2
  port (
       X : in STD_LOGIC_VECTOR(31 downto 0);
       Y0 : out STD_LOGIC_VECTOR(3 downto 0);
       Y1 : out STD_LOGIC_VECTOR(3 downto 0);
       Y2 : out STD_LOGIC_VECTOR(3 downto 0);
       Y3 : out STD_LOGIC_VECTOR(3 downto 0);
       Y4 : out STD_LOGIC_VECTOR(3 downto 0);
       Y5 : out STD_LOGIC_VECTOR(3 downto 0);
       Y6 : out STD_LOGIC_VECTOR(3 downto 0);
       Y7 : out STD_LOGIC_VECTOR(3 downto 0)
  );
end component;
component Fub3
  port (
       X : in STD_LOGIC_VECTOR(3 downto 0);
       Y : out STD_LOGIC_VECTOR(6 downto 0)
  );
end component;

---- Signal declarations used on the diagram ----

signal BUS168 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS174 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS180 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS182 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS188 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS190 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS192 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS194 : STD_LOGIC_VECTOR(3 downto 0);
signal BUS52 : STD_LOGIC_VECTOR(31 downto 0);

begin

----  Component instantiations  ----

U1 : Fub1
  port map(
       CLK => CLK,
       WE => WE,
       X => X,
       Y => BUS52
  );

U10 : Fub3
  port map(
       X => BUS194,
       Y => LCD0
  );

U2 : Fub2
  port map(
       X => BUS52,
       Y0 => BUS194,
       Y1 => BUS192,
       Y2 => BUS190,
       Y3 => BUS188,
       Y4 => BUS182,
       Y5 => BUS180,
       Y6 => BUS174,
       Y7 => BUS168
  );

U3 : Fub3
  port map(
       X => BUS168,
       Y => LCD7
  );

U4 : Fub3
  port map(
       X => BUS180,
       Y => LCD5
  );

U5 : Fub3
  port map(
       X => BUS188,
       Y => LCD3
  );

U6 : Fub3
  port map(
       X => BUS192,
       Y => LCD1
  );

U7 : Fub3
  port map(
       X => BUS174,
       Y => LCD6
  );

U8 : Fub3
  port map(
       X => BUS182,
       Y => LCD4
  );

U9 : Fub3
  port map(
       X => BUS190,
       Y => LCD2
  );


end lab8_2;
