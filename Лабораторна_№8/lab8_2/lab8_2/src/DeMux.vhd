-------------------------------------------------------------------------------
--
-- Title       : Fub2
-- Design      : lab8_2
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab8_2\lab8_2\src\DeMux.vhd
-- Generated   : Tue May 21 20:43:40 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Fub2} architecture {DeMux}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Fub2 is
	 port(
		 X : in STD_LOGIC_VECTOR(31 downto 0);
		 Y7 : out STD_LOGIC_VECTOR(3 downto 0);
		 Y6 : out STD_LOGIC_VECTOR(3 downto 0);
		 Y5 : out STD_LOGIC_VECTOR(3 downto 0);
		 Y4 : out STD_LOGIC_VECTOR(3 downto 0);
		 Y3 : out STD_LOGIC_VECTOR(3 downto 0);
		 Y2 : out STD_LOGIC_VECTOR(3 downto 0);
		 Y1 : out STD_LOGIC_VECTOR(3 downto 0);
		 Y0 : out STD_LOGIC_VECTOR(3 downto 0)
	     );
end Fub2;

--}} End of automatically maintained section

architecture DeMux of Fub2 is
begin

			Y0 <= X(3 downto 0);
			Y1 <= X(7 downto 4);
			Y2 <= X(11 downto 8);
			Y3 <= X(15 downto 12);
			Y4 <= X(19 downto 16);
			Y5 <= X(23 downto 20);
			Y6 <= X(27 downto 24);
			Y7 <= X(31 downto 28);	

end DeMux;
