-------------------------------------------------------------------------------
--
-- Title       : Fub3
-- Design      : lab8_2
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab8_2\lab8_2\src\LCDConv.vhd
-- Generated   : Tue May 21 20:44:14 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Fub3} architecture {LCDConv}}

library IEEE;
use ieee.std_logic_1164.all;

entity Fub3 is
	 port(
		 X : in STD_LOGIC_VECTOR(3 downto 0);
		 Y : out STD_LOGIC_VECTOR(6 downto 0)
	     );
end Fub3;

--}} End of automatically maintained section

architecture LCDConv of Fub3 is
begin

	with x select
	Y <= "1110111" when "0000",
	"0100100" when  "0001",
	"1011101" when 	"0010",
	"1101101" when "0011",
	"0101110"  when   "0100",
	"1101011" when 	"0101",
	"1111011" when  "0110",
	"0010101"  when "0111",
	"1111111" when 	  "1000",
	"1111011" when 	 "1001",
	"0000000" when 	 others;  

end LCDConv;
