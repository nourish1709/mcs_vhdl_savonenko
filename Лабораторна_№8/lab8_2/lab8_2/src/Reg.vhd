-------------------------------------------------------------------------------
--
-- Title       : Fub1
-- Design      : lab8_2
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab8_2\lab8_2\src\Reg.vhd
-- Generated   : Tue May 21 20:42:29 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Fub1} architecture {Reg}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Fub1 is
	 port(
		 X : in STD_LOGIC_VECTOR(31 downto 0);
		 WE : in STD_LOGIC;
		 CLK : in STD_LOGIC;
		 Y : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end Fub1;

--}} End of automatically maintained section

architecture Reg of Fub1 is
begin

process (CLK)
		variable temp : std_logic_vector(31 downto 0):="00000000000000000000000000000000";
	begin
		if rising_edge(CLK) then			
			
			if WE = '1'  then
				temp := X;
			else 
				Y <= temp;
			end if;			
			
		end if; 
	end process;


end Reg;
